/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author aaj001
 *
 */
public class Fahrenheit extends Temperature {
	public Fahrenheit(float t) {
		super(t);
	}
	public String toString() {
		// TODO Complete this method
		return "" + getValue() + " F";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		float C = (this.getValue() - 32)/(float)1.8;
		return new Celsius(C);
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return this;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		float K = ((this.getValue()-32)/(float)1.8)+273;
		return new Kelvin(K);
	}
}
