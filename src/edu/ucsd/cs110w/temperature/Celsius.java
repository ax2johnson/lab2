/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author aaj001
 *
 */
public class Celsius extends Temperature {
	public Celsius(float t) {
		super(t);
	}
	public String toString() {
		// TODO Complete this method
		return "" + getValue() + " C";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float F = this.getValue()*(float)1.8+32;
		return new Fahrenheit(F);
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		float K = this.getValue()+273;
		return new Kelvin(K);
	}
}
