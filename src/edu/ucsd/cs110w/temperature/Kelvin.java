/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (aaj001): write class javadoc
 *
 * @author aaj001
 *
 */
public class Kelvin extends Temperature {
	public Kelvin(float t) {
		super(t);
	}
	public String toString() {
		return "" + getValue() + " K";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		float C = this.getValue()-273;
		return new Celsius(C);
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float F = (float)1.8*(this.getValue()-273)+32;
		return new Fahrenheit(F);
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return this;
	}
	
}
